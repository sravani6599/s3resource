module "s3" {
   source = "../Modules"
  #  bucket name should be unique
  bucket_name = "srvbuc" 
   #dynamo_table = "terraform_store"
  #instance_type = "t2.micro"  
  region = "us-west-2"

  availability_zones = "us-west-2a"
}