variable "bucket_name" {
    default = "srvbuc"
}

#variable "acl_value" {
   # default = "public-read-write"
#}

variable "region" {
  description = "Region"
}

variable "availability_zones" {
  description = "Availability zones that are going to be used for the subnets"
}
